/* eslint-disable prettier/prettier */
import axios from "axios";
import { Voto } from "../controllers/eleitor.controller";

type Log = {
  id: number;
  titulo: number;
  votou: boolean;
}

const request = (url: any, method: any, data: any) => {
  return axios({ url, method, data });
};

class RequisicoesEleitor {
  private PORT = process.env.PORT_JSON_SERVER;
  
  public async autenticacao(titulo: number): Promise<boolean> {
    let res: boolean = false;
    const url: string = `http://localhost:${this.PORT}`;
    const sim: boolean = true;
    const jaVotou = await request(`${url}/log?titulo=${titulo}&votou=${sim}`, "get", null);
    
    if (jaVotou.data[0]) {
      res = true;
    } else {
      const obj: Log = {
        id: null,
        titulo: titulo,
        votou: false
      }
        
      await request(`${url}/log`, "post", obj);
    }

    return res;
  }

  public async votar(voto: Voto, titulo: number): Promise<boolean> {
    let response: boolean = false;
    
    try {
      const url: string = `http://localhost:${this.PORT}`;
      await request(`${url}/votos`, "post", voto);
      
      const eleitor = await request(`${url}/log?titulo=${titulo}`, "get", null);
        
      await request(`${url}/log/${eleitor.data[0].id}`,"patch", {votou: true});
      
      response = true;
    } catch (err) {
      console.log(err);
    }
    return response;
  }
}

export default new RequisicoesEleitor();
