import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CandidatosService {

  baseUrl = environment.baseUrl;

constructor( private http: HttpClient) { }

getCandidato(numero: number): Observable<any>{
  return this.http.get(`${this.baseUrl}/candidato/${numero}`)
}
}
