/* eslint-disable prettier/prettier */
import express from "express";
import EleitorController from "../controllers/eleitor.controller";

const eleitorRoutes = express.Router();

eleitorRoutes.get("/autenticacao/:titulo", EleitorController.autenticacao);
eleitorRoutes.post("/votar", EleitorController.votar);

export default eleitorRoutes;
