/* eslint-disable prettier/prettier */
import RequestCandidate from "../data/request.candidate";
import { Candidate } from "../controllers/candidate.controller";

class CandidateService {
  async find(number: number): Promise<Candidate> {
    const candidate = await RequestCandidate.find(number);

    return candidate;
  } 
  
}

export default new CandidateService();
