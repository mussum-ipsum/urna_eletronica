/* eslint-disable prettier/prettier */
import { Request, Response } from "express";
import ServiceEleitor from "../services/eleitor.service";

export type Voto = {
  id: number | null;
  numero: number;
  tituloEleitor?: number;
  nome: string;
  partido: string;
};
class EleitorController {

  static title: string;
  
  async autenticacao(req: Request, res: Response) {
    try {
      const titulo = req.params.titulo;
      EleitorController.title = titulo;
      const response = await ServiceEleitor.autenticacao(Number(titulo));

      res.status(200).json({ titulo: response });
    } catch (error) {
      res.status(400).json({ error: error });
    }
  }

  async votar(req: Request, res: Response) {
    try {
      const { id, numero, nome, partido, titulo } = req.body;
      console.log(numero);
      
      const voto: Voto = {
        id: id,
        numero: nome === "" ? "nulo" : numero,
        tituloEleitor: titulo,
        nome: nome,
        partido: partido,
      };

      const votou = await ServiceEleitor.votar(voto, +EleitorController.title);
      if (votou) res.status(200).json({ message: "Voto registrado" });
      
      else res.status(200).json({ message: "Erro no registro!" });
    } catch (error) {
      res.status(400).json({ error: error });
    }
  }  
}

export default new EleitorController();
