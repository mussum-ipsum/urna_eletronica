import { TelaComponentComponent } from './componentes/tela-component/tela-component.component';
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AutenticacaoComponent } from "./componentes/autenticacao/autenticacao.component";
import { LoginComponent } from "./login/login.component";
import { ResultadosComponent } from "./componentes/resultados/resultados.component";
import { AuthGuard } from './componentes/guards/auth.guard';


export const routes: Routes = [
  {path: "", redirectTo: "autenticacao/titulo", pathMatch: "full"},
  {path: "autenticacao/:titulo", component:AutenticacaoComponent },
  {path: "tela", component: TelaComponentComponent},
  {path: "resultados", component: ResultadosComponent,canActivate: [AuthGuard] },
  {path: "login", component: LoginComponent},
  {path: "resultados", component: ResultadosComponent},
  {path: "login", component: LoginComponent}

]


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule{

}
