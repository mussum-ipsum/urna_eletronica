/* eslint-disable prettier/prettier */
export class Apuracao {
  private _id: number;
  private _nome: string;
  private _foto: string;
  private _partido: string;
  private _votos: number;  
  private _porcentVotos: number;

  constructor(apuracao: Apuracao) {
    this._id = apuracao.id;
    this._nome = apuracao.nome;
    this._foto = apuracao.foto;
    this._partido = apuracao.partido;
    this._votos = apuracao.votos;
    this._porcentVotos = apuracao._porcentVotos;
  }

  public get id(): number {
    return this._id;
  }

  public set id(id: number) {
    this._id = id;
  }

  public get nome(): string {
    return this._nome;
  }

  public set nome(nome: string) {
    this._nome = nome;
  }

  public get foto(): string {
    return this._foto;
  }

  public set foto(foto: string) {
    this._foto = foto;
  }

  public get partido(): string {
    return this._partido;
  }

  public set partido(partido: string) {
    this._partido = partido;
  }

  public get votos(): number {
    return this._votos;
  }

  public set votos(votos: number) {
    this._votos = votos;
  }

  public get porcentVotos(): number {
    return this._porcentVotos;
  }

  public set porcentVotos(porcentVotos: number) {
    this._porcentVotos = porcentVotos;
  }
}
