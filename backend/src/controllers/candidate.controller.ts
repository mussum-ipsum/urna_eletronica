/* eslint-disable prettier/prettier */
import { Request, Response } from "express";
import CandidateService from "../services/candidate.service";

export type Candidate = {
  numero: number;
  nome: string;
  foto: string;
  partido: string;
};

class CandidatoController {
  async findOne(req: Request, res: Response) {
    try {
      const number = req.params.numero;
      
      const data: Candidate = await CandidateService.find(+number);
      res.status(200).json({ candidate: data });
    } catch (err) {
      res.status(400).json({ error: err });
    }
  } 
}

export default new CandidatoController();
