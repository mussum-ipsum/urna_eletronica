import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Resultados } from 'models/resultados.models';

@Injectable({
  providedIn: 'root'
})
export class ResultadoService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  public getResultados(): Observable<Resultados[]> {
    return this.http.get<Resultados[]>(`${this.baseUrl}/apuracao`);
  }

}
