import { Usuario } from './usuario';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private usuarioAutenticado : boolean = false;

  constructor(private router: Router) { }

  fazerLogin(usuario: Usuario){

    if(usuario.nome === 'admin@admin' &&
    usuario.senha === 'admin'){

      this.usuarioAutenticado = true;
      this.router.navigate(['/resultados'])

    } else {
      this.usuarioAutenticado = false;
      this.router.navigate(['/login'])
      alert("Login e/ou Senha incorreta")

    }
  }

  usuarioEstaAutenticado(): Observable<boolean> | boolean {
    return this.usuarioAutenticado;

  }
}
