import { ApuracaovotosService } from './service/apuracao/apuracaovotos.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Eleitor } from 'models/voto.models';
import { ConferenciatituloService } from './service/conferencia/conferenciatitulo.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'urna';

  eleitores : Eleitor [] = []
  constructor(private service: ApuracaovotosService, private route : ActivatedRoute, private service2: ConferenciatituloService){

  }
  ngOnInit(): void{
  }



}
