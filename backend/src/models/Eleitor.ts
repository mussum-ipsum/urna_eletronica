/* eslint-disable prettier/prettier */
export class Eleitor {

  private _id: number;
  private _nome: string;
  private _rg: number;
  private _titulo: number;

  constructor(eleitor: Eleitor) {
    this._id = eleitor.id;
    this._nome = eleitor.nome;
    this._rg = eleitor.rg;
    this._titulo = eleitor.titulo
  }

  public get id(): number {
    return this._id;
  }

  public set id(id: number) {
    this._id = id;
  }

  public get nome(): string {
    return this._nome;
  }

  public set nome(nome: string) {
    this._nome = nome;
  }

  public get rg(): number {
    return this._rg;
  }

  public set rg(rg: number) {
    this._rg = rg;
  }

  public get titulo(): number {
    return this._titulo;
  }

  public set titulo(titulo: number) {
    this._titulo = titulo;
  }

}