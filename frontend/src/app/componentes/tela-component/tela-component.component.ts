import { Candidate } from './../../../../../backend/src/controllers/candidate.controller';
import { CandidatosService } from './../../service/candidatos/candidatos.service';
import { TelaVotacaoService } from './../../service/telaVotacao/telaVotacao.service';
import { Component, OnInit } from '@angular/core';
import { ConferenciatituloService } from 'src/app/service/conferencia/conferenciatitulo.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tela-component',
  templateUrl: './tela-component.component.html',
  styleUrls: ['./tela-component.component.scss'],
})
export class TelaComponentComponent implements OnInit {

  public title = "e-Urna";

  numVotacao: string = '';
  isVisible = true;
  isVisibleTela = true;
  resposta: any = {
    retornoNome: '',
    retornoFoto: '',
    retornoNumeroCand: '',
    retornoNomePartido: '',
    retornoBranco: '',
  };

  constructor(
    private service: TelaVotacaoService,
    private serviceCandidatos: CandidatosService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  public clicou(num: string) {
    var caracteresVoto = this.numVotacao.length;

    if (caracteresVoto <= 1) {
      this.numVotacao += num;
    }
    if (caracteresVoto == 1) {
      const objCandidatos = this.serviceCandidatos
        .getCandidato(+this.numVotacao)
        .subscribe((res) => {
          const { foto, nome, eleitor, partido } = res.candidate;
          this.resposta.retornoFoto = foto;
          this.resposta.retornoNome = nome;
          this.resposta.retornoNomePartido = partido;
        });
    }
  }

  public branco() {
    this.isVisible = false;
    this.numVotacao = 'branco';
    this.resposta.retornoNome = 'branco';
    this.resposta.retornoNomePartido = 'branco';
    this.resposta.retornoBranco = 'Você está votando em Branco';
  }

  public corrige() {
    this.resposta.retornoNumeroCand = '';
    this.resposta.retornoNomePartido = '';
    this.resposta.retornoNome = '';
    this.resposta.retornoFoto = '';
    this.numVotacao = '';
    this.resposta.retornoBranco = '';
    this.isVisible = true;
  }

  public confirma() {
    this.isVisibleTela = false;

    const votoObj = {
      numero: this.numVotacao,
      nome: this.resposta.retornoNome,
      partido: this.resposta.retornoNomePartido,
    };

    this.service.postVoto(votoObj).subscribe(() => {
      this.resposta.retornoFim = 'FIM!';

      setTimeout(() => {
        this.router.navigate(['/']);
      },3000);
    });
  }
}
