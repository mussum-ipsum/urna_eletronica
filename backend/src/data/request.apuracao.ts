/* eslint-disable prettier/prettier */
import axios from "axios";
import { Voto } from "../controllers/eleitor.controller";

const request = (url: string, method: any, data: any) =>
  axios({ url, method, data });

class RequestApuracao {
  private PORT = process.env.PORT_JSON_SERVER;

  async apuracao(): Promise<any> {
    const apuracao = await request(
      `http://localhost:${this.PORT}/votos`,
      "get",
      null
    );
    // eslint-disable-next-line prettier/prettier

    const list: Voto[] = apuracao.data;
    let total = 0;

    const resp = list.reduce((object, item) => {
      const { numero } = item;

      if (!object[numero]) {
        object[numero] = 1;
      } else {
        object[numero]++;
      }
      total++;
      return object;
    }, {});
    
    const novoArr: Object[] = [];
    novoArr.push(resp);

    const resultado = await request(
      `http://localhost:${this.PORT}/candidatos`,
      "get",
      null
    );

    const buscar = (number: string): string => {
      let votos = "0";
      for (const property in resp) {
        if (property === number) {
          votos = `${resp[property]}`;
        }
      }
      return votos;
    };
    const table: any[] = [];

    resultado.data.forEach((element, i) => {
      const obj: any = {
        nome: element.nome,
        numero: element.numero,
        partido: element.partido,
        foto: element.foto,
        votos: Number(buscar(element.numero)),
        percenVotos: (Number(buscar(element.numero)) / total * 100).toFixed(2),
      };

      table.push(obj)
    });

    return table.sort((a, b) => (b.votos !== a.votos) ? b.votos - a.votos : a.nome - b.nome);
  }
}

export default new RequestApuracao();
