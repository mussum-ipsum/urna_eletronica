/* eslint-disable prettier/prettier */
export class EleitorExceptionVoter {
  private status: number;
  private msg: string;

  constructor(status: number, msg: string) {
    this.status = status;
    this.msg = msg;
  }

  public getStatus(): number {
    return this.status;
  }

  public setStatus(status: number) {
    return (this.status = status);
  }

  public getMsg(): string {
    return this.msg;
  }

  public setMsg(msg: string) {
    return (this.msg = msg);
  }
}
