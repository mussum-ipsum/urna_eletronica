import { AppRoutingModule } from './app-routing-modules';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { TelaComponentComponent } from './componentes/tela-component/tela-component.component';
import { AutenticacaoComponent } from './componentes/autenticacao/autenticacao.component';
import { ResultadosComponent } from './componentes/resultados/resultados.component';
import { LoginComponent } from './login/login.component';
import { AuthService } from './login/auth.service';
import { AuthGuard } from './componentes/guards/auth.guard';



@NgModule({
  declarations: [
    AppComponent,
    TelaComponentComponent,
    AutenticacaoComponent,
    LoginComponent,
    ResultadosComponent
    ],

  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
