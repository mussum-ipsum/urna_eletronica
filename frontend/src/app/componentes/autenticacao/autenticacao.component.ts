import { Component,  EventEmitter,  Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Eleitor } from 'models/voto.models';
import { ConferenciatituloService } from '../../service/conferencia/conferenciatitulo.service';
import { AutenticacaoService } from '../../service/autenticacao/autenticacao.service';

@Component({
  selector: 'app-autenticacao',
  templateUrl: './autenticacao.component.html',
  styleUrls: ['./autenticacao.component.scss']
})
export class AutenticacaoComponent{
  public title = "e-Urna";

  @Output() autenticar = new EventEmitter<any>()

  eleitores : Eleitor [] = []
  numVotacao : string = ""
  resposta: any = {
    retornoNome: "",
    retornoFoto: "",
    retornoNumeroCand: "",
    retornoNomePartido: "",
    retornoBranco : ""
  }

  constructor(private service: AutenticacaoService, private service2: ConferenciatituloService , private route : ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    // this.getByTitulo2()
  }


  public btnAcessUrna(){
    const autenticacao = {titulo: +this.numVotacao}
    const titulo = this.route.snapshot.paramMap.get('titulo')

     if(titulo !=null){
      this.service2.getByTitulo(+this.numVotacao).subscribe((res)=>{
        if(res.titulo){
          alert("Eleitor já votou anteriormente!")
        } else if(!res.titulo){
          this.router.navigate(['/tela'])
        }
      }, err =>{
        if(err.status === 400){
          console.log(err);
        }
      })
    }



  }

  public clicou(num: string){

    var caracteresVoto = this.numVotacao.length

    if (caracteresVoto < 12 ){
      this.numVotacao += num
    } else{
    }

    Number(this.numVotacao)


  }

  public corrige() {


    this.numVotacao = ''

  }
}
