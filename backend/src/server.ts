/* eslint-disable prettier/prettier */
import express from "express";
import path from 'path';
import routes from "./routes";

const cors = require("cors");
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(routes);

app.use('/assets/img', express.static(path.resolve(__dirname, '..', 'assets/img')));

const PORT_NODE = process.env.PORT_SERVER_NODE;

app.listen(PORT_NODE, () => {
  console.log(`server is up at ${PORT_NODE}`);
});
