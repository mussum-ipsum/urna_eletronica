/* eslint-disable prettier/prettier */
import { Candidate } from "../controllers/candidate.controller";
import axios from "axios";

const request = (url: string, method: any, data: any) =>
  axios({ url, method, data });

  class RequestCandidate {
  private PORT = process.env.PORT_JSON_SERVER;
  async find(number: number): Promise<Candidate> {

    const candidates = await request(`http://localhost:${this.PORT}/candidatos`, "get", null );

    // eslint-disable-next-line prettier/prettier
    const response: Candidate = candidates.data.find((candidate: { numero: string; }) => candidate.numero === String(number));

    return response;
  } 
}

export default new RequestCandidate();
