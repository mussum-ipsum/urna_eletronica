import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Eleitor, Voto } from '../../../../models/voto.models';


@Injectable({
  providedIn: 'root'
})

export class ApuracaovotosService {
private _listaVotos: any [] = []
private url = "http://localhost:3000/votos"
constructor(private httpClient: HttpClient) { }

public getAllVoto() : Observable<Voto[]>{
  return this.httpClient.get<Voto[]>(this.url)
}

addVoto(voto: any): Observable<Voto>{
  this.plotarData(voto)
  return this.httpClient.post<Voto>(this.url, voto)
}


public get listaVotos() : any []{
  return this._listaVotos
}


public set listaVotos(v : any[]) {
  this._listaVotos = v;
}

private plotarData (voto: any){
  voto.data = new Date
}


}


