/* eslint-disable prettier/prettier */
import { Router } from "express";
import CandidatoController from "../controllers/candidate.controller";

const candidateRouter = Router();

candidateRouter.get("/:numero", CandidatoController.findOne);

export default candidateRouter;
