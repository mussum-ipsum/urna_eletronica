import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DadosVotos, Resultados } from 'models/resultados.models';
import { ResultadoService } from 'src/app/service/resultado/resultado.service';
import { ApuracaovotosService } from '../../service/apuracao/apuracaovotos.service';

@Component({
  selector: 'app-resultados',
  templateUrl: './resultados.component.html',
  styleUrls: ['./resultados.component.scss'],
})
export class ResultadosComponent implements OnInit {
  resultados: Resultados[] = [];
  dadosVotos : DadosVotos[] = [];

  title = "Apuração";

  constructor(private service: ResultadoService) {}

  ngOnInit(): void {
    this.exibirResultados();
  }


  exibirResultados() {

    this.service.getResultados().subscribe((res) => {
      this.resultados = res
    }, err => {
      console.log(err);
    });

  }}
