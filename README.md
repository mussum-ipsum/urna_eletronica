e-Urna
===============================


![Badge](https://img.shields.io/github/license/sidneymiranda/project-proffy)
![Badge](https://img.shields.io/github/release/sidneymiranda/project-proffy)


## :green_book: Sobre

Este projeto foi baseado na urna eletrônica brasileira utilizada nos eventos eleitorais. Inicialmente a urna projetada para eleições presidenciais, cujo eleitor deve entrar com o título na tela inicial, assumindo o papel do mesário, para então a aplicação efetuar a autenticação do mesmo e permitir a entrada do número de seu candidato.


## :tools: Gestão do Projeto

Optamos pelo [![trello](https://img.shields.io/badge/trello-blue)](https://trello.com/b/puuRXmbp/projeto-capgemini) como solução para o gerenciarmento do projeto utilizando-se da abordagem do Kanban e Scrum


## :pencil: Como utilizar

Para clonar e executar esta aplicação você precisará do [![git](https://img.shields.io/badge/git-2.32.0.windows.2-orange)](https://git-scm.com/), [![node](https://img.shields.io/badge/node-v14.17.16-green)](https://nodejs.org/) ou superior [![npm](https://img.shields.io/badge/npm-v6.14.15-blue)](https://www.npmjs.com) ou outro gerenciador de pacotes instalado em seu computado.

Na linha de comando:


``` shell
# Clone o respositório
$ git clone https://sidney_miranda@bitbucket.org/mussum-ipsum/urna_eletronica.git

# Na raiz do projeto, entre no diretório:
$ cd backend

# Instale as dependências da API:
$ npm install

# Inicie o servidor NODE e json-server
$ npm run dev
$ npm run json

# Na raiz do projeto, entre no diretório:
$ cd frontend

# Instale as dependências da aplicação Angular:
$ npm install

# Inicie a aplicação
$ ng serve -o

```

## :rocket: Tecnologias 

O projeto foi desenvolvido utilizando as tecnologias:

### API REST
- [JSON Server](https://www.npmjs.com/package/json-server)

### BACKEND
- [Node.JS](https://nodejs.org)
- [TypeScript](https://www.typescriptlang.org)
- [Express](https://expressjs.com)


### FRONTEND
- [Angular](https://angular.io)
- [Typescript](https://www.typescriptlang.org)
- [Bootstrap](https://getbootstrap.com)
- [Axios](https://axios-http.com)


## :link: Como contribuir 

- Faça um fork do repositório
- Crie uma branch e trabalhe em cima da nova funcionalidade
- Faça um commit
- Envie sua branch para o repositório remoto
- Vá até Pull Requests, em seu projeto, e abra uma solicitação explicando o que desenvolveu.


## :heavy_check_mark: Licença 

This project is under the MIT license. See the [LICENSE](https://bitbucket.org/mussum-ipsum/urna_eletronica/src/master/LICENSE.txt) file for more details.

---
###### :computer: Desenvolvido por:
 - [Sidney Miranda](https://www.linkedin.com/in/sidney-miranda)
 - [Barbara Veiga](https://www.linkedin.com/in/barbara-moreira-veiga-4841ab172)
 - [Lucas Lousada](https://www.linkedin.com/in/lucas-lousada-rodrigues)
 - [Tiago Perroni](https://www.linkedin.com/in/tiago-perroni-479009193)
 - [Bruno Melo](https://www.linkedin.com/in/brunomello2000)
