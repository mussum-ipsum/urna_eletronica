import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Eleitor } from 'models/voto.models';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class AutenticacaoService{
  private _listaEleitores : any [] = [];
  baseUrl = environment.baseUrl;

  constructor (private httpClient: HttpClient){}

  public getAllEleitores(): Observable<Eleitor[]>{
    return this.httpClient.get<Eleitor[]>(`${this.baseUrl}/eleitor/autenticacao`)
  }

  addEleitor(eleitor: any): Observable<Eleitor>{
    return this.httpClient.post<Eleitor>(this.baseUrl, eleitor)
  }

  public get listaEleitores(): any []{
    return this._listaEleitores
  }


  public set listaEleitores(v : any[]) {
    this._listaEleitores = v;
  }

}

