/* eslint-disable prettier/prettier */
import RequestApuracao from "../data/request.apuracao";
import { Apuracao } from "../models/apuracao";

class ApuracaoService {  
  
  async apuracao(): Promise<Apuracao> {   
    
    return await RequestApuracao.apuracao();
  }
}

export default new ApuracaoService();
