import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TelaVotacaoService {

constructor(private http: HttpClient) { }

baseUrl = environment.baseUrl;

postVoto(voto: object):Observable<any>{
  return this.http.post(`${this.baseUrl}/eleitor/votar`, voto)
}
}
