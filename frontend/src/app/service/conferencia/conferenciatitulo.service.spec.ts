/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ConferenciatituloService } from './conferenciatitulo.service';

describe('Service: Conferenciatitulo', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConferenciatituloService]
    });
  });

  it('should ...', inject([ConferenciatituloService], (service: ConferenciatituloService) => {
    expect(service).toBeTruthy();
  }));
});
