import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConferenciatituloService {

  baseUrl = environment.baseUrl;

constructor( private http: HttpClient) { }

getByTitulo(titulo: number): Observable<any>{
  return this.http.get(`${this.baseUrl}/eleitor/autenticacao/${titulo}`)
}
}
