/* eslint-disable prettier/prettier */
import requisicoesEleitor from "../data/req.data";
import { Voto } from "../controllers/eleitor.controller";

class ServiceEleitor {
  
  async autenticacao(titulo: number): Promise<boolean> {
    const getEleitor = await requisicoesEleitor.autenticacao(titulo);
    return getEleitor;
  }

  async votar(voto: Voto, titulo: number): Promise<boolean> {
    const response: boolean = await requisicoesEleitor.votar(voto, titulo);
    return response;
  }

}

export default new ServiceEleitor();
