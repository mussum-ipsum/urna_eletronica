/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TelaVotacaoService } from './telaVotacao.service';

describe('Service: TelaVotacao', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TelaVotacaoService]
    });
  });

  it('should ...', inject([TelaVotacaoService], (service: TelaVotacaoService) => {
    expect(service).toBeTruthy();
  }));
});
