/* eslint-disable prettier/prettier */
import { Router } from "express";
import ApuracaoController from "../controllers/apuracao.controller";

const candidateRouter = Router();

candidateRouter.get("/", ApuracaoController.apuracao);

export default candidateRouter;
