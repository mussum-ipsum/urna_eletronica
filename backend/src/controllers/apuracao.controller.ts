/* eslint-disable prettier/prettier */
import { Request, Response } from "express";
import apuracaoService from "../services/apuracao.service";


class ApuracaoController {  

  async apuracao(req: Request, res: Response) {
    try {
      const response = await apuracaoService.apuracao();
      res.status(200).json(response);
    } catch (err) {
      res.status(400).json({ error: err });
    }
  }
}

export default new ApuracaoController();
