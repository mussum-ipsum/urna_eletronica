export interface Resultados {
  id?: number;
  nome: string;
  numero: string;
  partido: string;
  foto: string;
  votos: number;
  percenVotos: number;

}
export interface DadosVotos {

  votosTotais?: number;
  votosValidos?: number;
  votosBrancos?: number;
  votosNulos?: number;
  porcentVotosValidos?: number;
  porcentVotosBrancos?: number;
  porcentVotosNulos?: number;

}
