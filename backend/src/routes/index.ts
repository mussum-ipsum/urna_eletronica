/* eslint-disable prettier/prettier */
import { Router } from "express";
import candidateRouter from "./candidate.routes";
import eleitorRoutes from "./eleitor.routes";
import apuracaoRoutes from "./apuracao.routes";

const routes = Router();

routes.use("/candidato", candidateRouter);
routes.use("/eleitor", eleitorRoutes);
routes.use("/apuracao", apuracaoRoutes);

export default routes;
