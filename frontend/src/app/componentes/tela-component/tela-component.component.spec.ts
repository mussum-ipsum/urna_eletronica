import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaComponentComponent } from './tela-component.component';

describe('TelaComponentComponent', () => {
  let component: TelaComponentComponent;
  let fixture: ComponentFixture<TelaComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TelaComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
